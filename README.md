# Projet YNOV : TicTacToe
Essayer de faire tout ce qui est décris ci dessous :

# Projet YNOV pour le cours de NLP : TicTacToe

L'objectif du projet est de jouer au TicTacToe contre un bot par commande vocale. Un échange sera, par exemple :

  - Utilisateur : "I want to play in the middle cell."
  - Bot : "Alright, I just played top-left."
  - Utilisateur : "For this turn, I choose left."
  - Bot : "Well done, you won the game !"
  - Utilisateur : "Thank you, now please start a new game."
  - Bot : "Ok, I restarted the game."
  - ...

Pour cela, nous nous appuierons sur les services suivants :

  - [AWS Lex](https://aws.amazon.com/fr/lex/) : L'agent conversationnel d'AWS
  - [AWS Lambda](https://aws.amazon.com/fr/lambda/features/) : Le FaaS d'AWS, qui nous permettra d'introduire de la logique dans la discussion.
  - [EC2](https://aws.amazon.com/fr/ec2/) : Le IaaS d'AWS, qui nous servira à heberger la logique du jeu.

## Lex : L'agent conversationnel

Allez sur la page du service Lex et créez un **Bot Custom**. Vous aurez besoin :

  - d'un **intent** pour comprendre quand l'utilisateur formulera une demande pour jouer une case.
  - d'un **slot** sur l'intent pour savoir quelle case l'utilisateur a joué.

Les réponses seront gérées par une fonction Lambda qu'il vous faudra définir.

## Lambda : La logique de discussion

Cette fonction sera en charge de récupérer les élements de la phrase de l'utilisateur, de les traiter et de retourner une réponse au Bot. Un exemple de code vous est fourni dans le fichier `nlp/lambda_function.py`.

Vous pouvez aller dans le service Lambda sur AWS et créer une nouvelle fonction. J'ai choisi python3.6 dans mon exemple, mais vous pouvez prendre le langage que vous préférez.

Dans un premier temps, occupez-vous simplement de sa création et assurez-vous qu'elle puisse envoyer une réponse qui sera prononcée par le Bot. Vous pouvez utiliser le module `requests` pour interroger l'API implémentant dans la logique du jeu.

## EC2 : La logique du jeu

Il va falloir créer une instance EC2 (J'ai pris une **Ubuntu 18.04**, une **t2.micro** sera suffisante pour notre usage.) contenant le code du fichier `app.py`. Ce code est une base exposant une API pour la logique du jeu, il gère :

  - Le maintien du tableau de jeu
  - La possibilité pour l'utilisateur de jouer un coup
  - La possibilité pour le robot de jouer un coup
  - La possibilité de réinitialiser le tableau.

Nous utilisons la librairie **Flask** en **Python**. Encore une fois, vous pouvez choisir d'utiliser un autre langage pour cela. 

Il faudra donc mettre le code sur l'instance, ouvrir le port et lancer le service d'API. Si vous choisissez de partir en python, voici les instructions pour parametrer l'instance.

Le code expose l'API sur le port 5001, il faudra donc l'ouvrir vers l'exterieur dans les **security-group** de l'EC2. Une autre possibilité est de placer un **role IAM** sur votre Lambda lui autorisant à utiliser les services EC2.

Puis parametrer l'environnement :

```bash
sudo apt-get update && sudo apt-get upgrade -y

sudo apt-get install -y python3-pip python3-dev
pip3 install --user pipenv
echo "PATH=$HOME/.local/bin:$PATH" >> ~/.bashrc
source ~/.bashrc

git clone https://gitlab.com/r_queraud_catie/cours-nlp-2-projet  # Ce projet
cd cours-nlp-2-projet/  # Ce projet

pipenv install
pipenv run python app.py
```

Pour développer directement sur l'EC2, vous pouvez utiliser le plugin `ssh fs` de Visual Studio Code. Vous pouvez également utiliser directement `nano` ou n'importe quel autre editeur directement sur l'EC2 si vous êtes à l'aise.

## Raccorder les différents élements

Maintenant que tout est en place, vous pouvez faire en sorte que le Chatbot fonctionne et qu'on puisse jouer au TicTacToe. Il y a du code à trou pour :

  - La fonction Lambda.
  - La logique du jeu sur le service Flask.

## Amélioration : un bot intelligent

Le bot joue au hasard sur les cases de tableau. Un des avantages du TicTacToe est qu'il est simple de ne perdre aucune partie et d'arriver sur des égalités.

Votre bot jouant au hasard, pour le moment, il est facile de gagner. Vous pouvez trouver une solution pour qu'il y ait toujours égalité si les deux entités jouent parfaitement ! Certaines familles d'algorithmes peuvent vous aider :

  - L'algorithme du [min-max](https://fr.wikipedia.org/wiki/Algorithme_minimax) et ses variantes négamax, élagage alpha-beta, ... efficace bien que long et couteux !
  - Les algorithmes génétiques
  - L'apprentissage par renforcement

# Cours Vision - Projet - 25/04/2019

Nous allons aborder la suite du projet démarré sur la partie NLP.

Avant toute chose, pensez à choisir un créneau pour votre groupe sur : https://doodle.com/poll/zzt9z8cy3ykdbcgs

L'objectif est d'incorporer la validation de l'état du damier avec notre robot. L'idée est de jouer une partie à l'oral contre notre robot, un déroulement serait ainsi :

  - Utilisateur : "I want to play in the middle cell." ** ==> L'utilisateur dessine une croix au milieu**
  - Bot : "Alright, I just played top-left."  ** ==> L'utilisateur dessine un rond en haut à gauche**
  - Bot : "I agree with your board state."
  - Utilisateur : "For this turn, I choose left." ** ==> L'utilisateur dessine une croix à droite **
  - Bot : "Oh, I think you made a mistake. You told me left but you drew your cross right... I'm a bit lost, can you please restart the game !"
  - ...

Dans l'idéal, au cours d'une partie, nous fixerions une caméra au-dessus d'un damier pour le filmer en continu. Notre robot pourrait ainsi confirmer qu'il est d'accord avec l'état ou emettre une opposition dans le cas d'un mauvais mouvement.

Le projet peut être découpé en plusieurs parties :

  1. Découpage du damier
  2. Classification de la case
  3. Reconnaissance du damier
  4. Intégration avec le chatbot

## Partie 1 : Découpage du damier

En partant d'une photo de damier rempli, extraire une image par case. On peut imaginer deux difficultées :
  - Une image de damier propre dessiné sous à l'ordinateur
  - Une photo d'un damier pris sur une feuille de papier ou un tableau blanc. Dans ce cas-là, il faudra certainement faire un seuillage pour binariser l'image.

Dans les deux cas, il sera nécessaire de détecter les traits du damier, on pourra par exemple :

  - Dans un premier temps supposer que les lignes du damier sont les valeurs extrêmes. C'est à dire qu'aucune croix ou rond ne sera ni plus haut, ni plus à gauche, ... que le trait de damier.
  - Dans un second temps, experimenter une détection de lignes avec OpenCV et/ou tenter de matcher la forme attendue...

## Partie 2 : Classification des cases

Cela necessite de générer un jeu de données d'entrainement et d'entrainer un modèle pour classifier nos cases en `cercle`, `croix` ou `vide`.

Il faudra donc :

  - Générer de la donnée d'entrainement
  - La labelliser
  - Faire de l'augmentation (rotations, translations, contraste, ...)
  - Choisir et entrainer un modèle de classification

## Partie 3 : Reconnaissance du damier

Mettre bout à bout les précedentes étapes en écrivant un script permettant, à partir d'une photo, de ressortir la composition du damier. 

En vue d'une future intégration, on peut également faire en sorte que le script se déclenche au moment où on l'on pousse une image. Deux solutions proposées :

  - Soit l'image est envoyée directement à notre API hébergeant la logique du jeu.
  - Soit on se sert du stockage d'AWS (S3) pour déposer notre image et déclencher une lambda pour traiter l'image.

## Partie 4 : Intégration avec le chatbot

Faire en sorte que le robot intéragisse avec le dessin et réagisse en conséquence. Il est possible dans une première étape de demander à l'utilisateur de prendre une photo et de l'envoyer au robot, mais une amélioration pourrait être d'utiliser la webcam et de demander au robot d'analyser en continu l'état du damier.

Le traitement sera possiblement trop long pour être traité par le chatbot. Dans ce cas, on peut imaginer faire appel au service text-2-speech d'AWS (Polly) pour générer une voix à côté qui ferait office "d'arbitre".

## Partie 5 : Proposer des améliorations

Le jeu est loin d'être parfait. Vous êtes libres de proposer des variations/améliorations pour améliorer l'expérience !
