from flask import Flask, request
from flask_restful import Api, Resource, reqparse
import sys, os
from tictactoe_reconizer.BordReconizer import BordReconizer
import cv2

[sys.path.append(i) for i in ['.', '..']]
from play import *

HUMAN = -1
COMPUTER = +1
NOT_PLAYED = 0


def format_value(value):
    if value == NOT_PLAYED:
        return "-"
    elif value == COMPUTER:
        return "O"
    return "X"


def convert_to_local_representation(game_state):
    converted = []
    for s in game_state:
        if s == "X":
            converted.append(HUMAN)
        elif s == "O":
            converted.append(COMPUTER)
        else:
            converted.append(NOT_PLAYED)
    return converted


class Board(Resource):
    board = [NOT_PLAYED] * 9

    def get(self):
        return {
                   "board": self.board
               }, 200

    def put(self):
        parser = reqparse.RequestParser()
        parser.add_argument("move")
        args = parser.parse_args()

        free_cells = empty_cells(self.board)
        player_move = int(args.move)
        if player_move in free_cells:
            self.board[player_move] = HUMAN
            computer_move = ai_turn(self.board)
            self.display()
        else:
            return {
                       "message": "The cell is already taken"
                   }, 400

        winner = self.get_winner_name()

        return {
                   "player_move": player_move,
                   "computer_move": computer_move,
                   "winner": winner,
                   "board": self.board
               }, 200

    def display(self):
        for index, value in enumerate(self.board):
            if index % 3 == 0:
                print("")
            print(format_value(value), end="|")
        print()

    def get_winner_name(self):
        if wins(self.board, COMPUTER):
            return "computer"
        elif wins(self.board, HUMAN):
            return "human"
        return "no one" if self.board.count(NOT_PLAYED) != 0 else "draw"

    def delete(self):
        for i in range(len(self.board)):
            self.board[i] = NOT_PLAYED
        return {
                   "board": self.board
               }, 200


class BoardImage(Resource):
    bord_reconizer = BordReconizer(epoch=150)
    PATH = "./upload"
    FILENAME = "file_to_analyse.png"

    def get(self):
        computer_move, game_state, winner = self.play_from_file()
        return {
                   "computer_move": computer_move,
                   "winner": winner,
                   "board": game_state
               }, 200

    def play_from_file(self):
        img = cv2.imread(os.path.join(app.config['UPLOAD_FOLDER'], self.FILENAME))
        img = cv2.resize(img, (600, 600))
        game_state = self.bord_reconizer.get_game_state(img)
        game_state = convert_to_local_representation(game_state)
        computer_move = ai_turn(game_state)
        self.display(game_state)
        winner = self.get_winner_name(game_state)
        return computer_move, game_state, winner

    def post(self):
        do_play_on_upload = self.get_play_parameter()

        if 'file' not in request.files:
            return {"message": "No file"}, 400
        file = request.files['file']
        if file.filename == '':
            return {"message": "Filename empty"}, 400
        if not file or not self.allowed_file(file.filename):
            return {"message": "Unknow error"}, 400

        file.save(os.path.join(app.config['UPLOAD_FOLDER'], self.FILENAME))
        body = {"message": "done"}
        if do_play_on_upload:
            computer_move, game_state, winner = self.play_from_file()
            body["computer_move"] = computer_move
            body["winner"] = winner
            body["board"] = game_state
        return body, 200

    def get_play_parameter(self):
        parser = reqparse.RequestParser()
        parser.add_argument("play")
        args = parser.parse_args()
        do_play_on_upload = args.play
        return do_play_on_upload

    def allowed_file(self, filename):
        return '.' in filename and \
               filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

    def display(self, board):
        for index, value in enumerate(board):
            if index % 3 == 0:
                print("")
            print(format_value(value), end="|")
        print()

    def get_winner_name(self, board):
        if wins(board, COMPUTER):
            return "computer"
        elif wins(board, HUMAN):
            return "human"
        return "no one" if board.count(NOT_PLAYED) != 0 else "draw"


if __name__ == "__main__":
    ALLOWED_EXTENSIONS = {'png', 'jpg'}

    app = Flask(__name__)
    app.config['UPLOAD_FOLDER'] = BoardImage.PATH

    api = Api(app)
    api.add_resource(Board, "/board")
    api.add_resource(BoardImage, "/analyse")

    app.run(
        host='0.0.0.0',
        port=5001,
        debug=True
    )
